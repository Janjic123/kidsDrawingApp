package com.example.kidsdrawingapp

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.media.MediaScannerConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_brush_size.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private var drawingView: DrawingView? = null
    private var mImageButtonCurrentPaint:ImageButton? = null
    var customProgresDialog: Dialog? = null

    private val openGaleryLauncher:ActivityResultLauncher <Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
                result ->

            if (result.resultCode == RESULT_OK && result.data != null){
                iv_background.setImageURI(result.data?.data)
            }
        }


    private val requestPermision: ActivityResultLauncher<Array<String>> =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
                permitions ->

            permitions.entries.forEach {
                val permisionName = it.key
                val IsGranted = it.value

                if (IsGranted) {
                    Toast.makeText(this,"Permision is granted and now you can storage the files",Toast.LENGTH_LONG).show()
                    val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    openGaleryLauncher.launch(pickIntent)
                } else {
                    if (permisionName == Manifest.permission.READ_EXTERNAL_STORAGE){
                        Toast.makeText(this,"Ooops you just denied permision",Toast.LENGTH_LONG).show()
                    }
                }
            }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        drawingView = findViewById(R.id.drawing_view)
        drawingView?.setSizeForBrush(40.toFloat())
        val lineralLayoutPaintColors = findViewById<LinearLayout>(R.id.ll_paint_colors)

        mImageButtonCurrentPaint = lineralLayoutPaintColors [1] as ImageButton
        mImageButtonCurrentPaint!!.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.palet_pressed))

        ib_brush.setOnClickListener{
            showBrushSizeChooserDialog()
        }


        ib_galery.setOnClickListener{
            requestStoragePermission()
        }

        ib_undo.setOnClickListener{
            drawingView?.onClickUndo()
        }

        ib_save.setOnClickListener{

            if (isReadStorageAllowed()){
                showCustomProgressDialog()
                lifecycleScope.launch{
                    val flDrawingView:FrameLayout = findViewById(R.id.fl_drawing_view_container)
                    saveBitMapFile(getBitMapFromView(flDrawingView))

                }
            }


        }



    }







    private fun showBrushSizeChooserDialog() {

        val brushDialog = Dialog(this)
        brushDialog.setContentView(R.layout.dialog_brush_size)

        val smallBtn = brushDialog.ib_small_brush
        smallBtn.setOnClickListener{
            drawingView?.setSizeForBrush(10.toFloat())
            brushDialog.dismiss()
        }

        val medium_btn = brushDialog.ib_medium_brush
        medium_btn.setOnClickListener{
            drawingView?.setSizeForBrush(20.toFloat())
            brushDialog.dismiss()
        }

        val large_btn = brushDialog.ib_large_brush
        large_btn.setOnClickListener{
            drawingView?.setSizeForBrush(30.toFloat())
            brushDialog.dismiss()
        }

        brushDialog.show()
    }


    fun painClicked(view:View){

       if(view != mImageButtonCurrentPaint){
           val imageButton = view as ImageButton
           val colorTag = imageButton.tag.toString()

           drawingView?.setColor(colorTag)
           imageButton.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.palet_pressed))

           mImageButtonCurrentPaint?.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.palet_normal))
           mImageButtonCurrentPaint = view
       }
    }


    private fun isReadStorageAllowed (): Boolean {
        val result = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE)

        return result == PackageManager.PERMISSION_GRANTED

    }



    private fun requestStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)){

            showRationaleDialog("Kids drawing App","Kids drawing app" + "needs to Access your External Storage")
        }else {
            requestPermision.launch(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE))
        }
    }


    private fun getBitMapFromView (view: View):Bitmap {
        val returnedBitmap = Bitmap.createBitmap(view.width,view.height,Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable = view.background
        if (bgDrawable != null){
            bgDrawable.draw(canvas)
        }else{
            canvas.drawColor(Color.WHITE)
        }
        view.draw(canvas)
        return returnedBitmap
    }


    private suspend fun saveBitMapFile (mBitmap: Bitmap?): String {
        var result = ""
        withContext(Dispatchers.IO){
            if (mBitmap != null) {
                try {
                    val bytes = ByteArrayOutputStream()
                    mBitmap.compress(Bitmap.CompressFormat.PNG,90,bytes)

                    val f = File(externalCacheDir?.absoluteFile.toString() + File.separator + "KidDrawing_" + System.currentTimeMillis() /1000 + "png")
                    val fo = FileOutputStream(f)

                    fo.write(bytes.toByteArray())
                    fo.close()

                    result = f.absolutePath

                    runOnUiThread{
                        cancelCustomProgessDialog()
                        if (result.isNotEmpty()){
                            Toast.makeText(this@MainActivity,"File saved sucessfuly: $result",Toast.LENGTH_LONG).show()
                            shareImage(result)

                        } else{
                            Toast.makeText(this@MainActivity,"Something went wrong with saving file",Toast.LENGTH_LONG).show()
                        }
                    }
                } catch (e: Exception) {

                    result = ""
                    e.printStackTrace()
                }
            }
        }
        return result
    }







    private fun showRationaleDialog (title:String,message:String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title).setMessage(message).setPositiveButton("Cancel")
        {
            dialog,_->
            dialog.dismiss()
        }
        builder.create().show()
    }


    private fun cancelCustomProgessDialog(){
        if (customProgresDialog != null){
            customProgresDialog?.dismiss()
            customProgresDialog = null
        }


    }


    private fun showCustomProgressDialog (){
        customProgresDialog = Dialog(this)
        customProgresDialog?.setContentView(R.layout.custom_progress_dialog)
        customProgresDialog?.show()
    }


    // result = path of the image
    private fun shareImage (result:String){
        MediaScannerConnection.scanFile(this, arrayOf(result),null){
            path,uri ->

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_STREAM,uri)
            shareIntent.type = "image/png"
            startActivity(Intent.createChooser(shareIntent,"Share"))

        }

    }







}